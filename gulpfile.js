const gulp = require('gulp');
const zip = require('gulp-zip');
var  clean = require('gulp-clean');
var noop = require("gulp-noop");

const packageInfo = require('./package.json');
 
exports.default = () => (
    gulp.src('src/**')
        .pipe(zip(packageInfo.name + '.ocmod.zip'))
        .pipe(gulp.dest('dist'))
);
exports.dev = () => (
    
    gulp.src('src/upload/*')
        .pipe(process.env.OC_ROOT ? gulp.dest(process.env.OC_ROOT): noop())
);
exports.clean = () => (
    gulp.src('dist', {read: false, allowEmpty:  true })
   .pipe(clean())
);